from django.db import IntegrityError
from rest_framework import generics, status, mixins
from rest_framework.response import Response

from waitlist.core.models import Restaurant, Waitlist, Entry
from waitlist.core.serializers import RestaurantSerializer, EntrySerializer, WaitlistSerializer, EntryDestroySerializer


class RestaurantApiView(generics.ListCreateAPIView):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class WaitlistEntryApiView(generics.RetrieveAPIView, generics.CreateAPIView):

    # get restaurant id
    # check if waitlist exists
    # create if the above is false
    # pass the waitlist obj to serializer
    # APIView to add relationship to the entries

    serializer_class = WaitlistSerializer

    def get_object(self):
        return generics.get_object_or_404(Waitlist, restaurant_id=self.kwargs['restaurant_id'])

    def create(self, request, *args, **kwargs):

        restaurant_id = kwargs.pop('restaurant_id')

        serializer = EntrySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        waitlist, _ = Waitlist.objects.get_or_create(restaurant_id=restaurant_id)
        serializer.validated_data.update({'waitlist': waitlist})
        try:
            self.perform_create(serializer)
        except IntegrityError:
            return Response({'Duplicate Email entry for this waitlist'}, status=status.HTTP_400_BAD_REQUEST)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class EntryApiView(mixins.DestroyModelMixin, generics.GenericAPIView):

    # change parent class method signature to
    # retrieve object via a custom filter
    # which accounts for restaurant/waitlist if
    # and email (passed as post data)

    serializer_class = EntryDestroySerializer

    def get_object(self, entry_email):
        return generics.get_object_or_404(Entry, waitlist__restaurant_id=self.kwargs['restaurant_id'], email=entry_email)

    def post(self, request, *args, **kwargs):
        entry_email = request.data['email']
        instance = self.get_object(entry_email)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
