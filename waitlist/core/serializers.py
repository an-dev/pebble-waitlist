from rest_framework import serializers

from waitlist.core.models import Restaurant, Entry, Waitlist


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ['name']


class EntryDestroySerializer(serializers.ModelSerializer):

    email = serializers.EmailField()

    class Meta:
        model = Entry
        fields = ['email']


class EntrySerializer(EntryDestroySerializer):

    name = serializers.CharField(max_length=64)

    class Meta(EntryDestroySerializer.Meta):
        fields = ['name', 'email']


class WaitlistSerializer(serializers.ModelSerializer):

    entries = EntrySerializer(many=True, read_only=True)

    class Meta:
        model = Waitlist
        fields = ['entries']
