from django.urls import path

from waitlist.core.views import RestaurantApiView, WaitlistEntryApiView, EntryApiView

urlpatterns = [
    path('restaurants/', RestaurantApiView.as_view(), name='core.restaurant-list-create'),
    path('restaurants/<int:restaurant_id>/waitlist/', WaitlistEntryApiView.as_view(), name='core.waitlist-get-create'),
    path('restaurants/<int:restaurant_id>/waitlist/entry/', EntryApiView.as_view(), name='core.entry-delete'),
]
