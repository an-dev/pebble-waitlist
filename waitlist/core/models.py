from django.db import models


class Restaurant(models.Model):
    name = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return f'Restaurant {self.name}'

    class Meta:
        ordering = ['id']


class Waitlist(models.Model):
    restaurant = models.OneToOneField(Restaurant, on_delete=models.CASCADE)

    def __str__(self):
        return f'Waitlist for {self.restaurant}'


class Entry(models.Model):
    name = models.CharField(max_length=64)
    email = models.EmailField()
    waitlist = models.ForeignKey(Waitlist, on_delete=models.CASCADE, related_name='entries')

    class Meta:
        unique_together = ('email', 'waitlist')
        ordering = ['id']

    def __str__(self):
        return f'{self.name} ({self.waitlist})'
