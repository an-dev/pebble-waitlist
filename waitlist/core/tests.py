from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APITransactionTestCase
from waitlist.core.models import Restaurant, Waitlist, Entry


# Restaurant Endpoints
class RestaurantTests(APITestCase):

    def setUp(self):
        self.url = reverse('core.restaurant-list-create')

    def test_create_restaurant_fails_no_data(self):
        response = self.client.post(self.url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Restaurant.objects.count(), 0)

    def test_create_restaurant_fails_duplicate(self):
        Restaurant.objects.create(name='foos restaurant')
        response = self.client.post(self.url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Restaurant.objects.count(), 1)

    def test_create_restaurant_success(self):
        response = self.client.post(self.url, {'name': 'bazs restaurant'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Restaurant.objects.count(), 1)

    def test_list_restaurant(self):
        Restaurant.objects.create(name='foos restaurant')
        Restaurant.objects.create(name='bazs restaurant')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], Restaurant.objects.count())


# Waitlist/Entry Endpoints
class WaitlistEntryTests(APITransactionTestCase):

    def setUp(self):
        self.restaurant = Restaurant.objects.create(name='foos restaurant')
        self.url = reverse('core.waitlist-get-create', args=[self.restaurant.id])

    def test_create_waitlist_no_restaurant(self):
        url = reverse('core.waitlist-get-create', args=[123])
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Waitlist.objects.count(), 0)
        self.assertEqual(Entry.objects.count(), 0)

    def test_create_waitlist_no_data(self):
        response = self.client.post(self.url, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Waitlist.objects.count(), 0)
        self.assertEqual(Entry.objects.count(), 0)

    def test_create_waitlist_name_only(self):
        response = self.client.post(self.url, {'name': 'john'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Waitlist.objects.count(), 0)
        self.assertEqual(Entry.objects.count(), 0)

    def test_create_waitlist_email_only(self):
        response = self.client.post(self.url, {'email': 'john.doe@example.com'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Waitlist.objects.count(), 0)
        self.assertEqual(Entry.objects.count(), 0)

    def test_create_waitlist_success(self):
        response = self.client.post(self.url, {'name': 'john doe', 'email': 'john.doe@example.com'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Waitlist.objects.count(), 1)
        self.assertEqual(Entry.objects.count(), 1)

    def test_create_waitlist_duplicate_entry(self):
        waitlist = Waitlist.objects.create(restaurant=self.restaurant)
        Entry.objects.create(waitlist=waitlist, name='john doe', email='john.doe@example.com')
        response = self.client.post(self.url, {'name': 'john doe', 'email': 'john.doe@example.com'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Waitlist.objects.count(), 1)
        self.assertEqual(Entry.objects.count(), 1)

    def test_retrieve_waitlist_success(self):
        Waitlist.objects.create(restaurant=self.restaurant)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Waitlist.objects.count(), 1)
        self.assertEqual(Entry.objects.count(), 0)

    def test_retrieve_waitlist_success_with_entries(self):
        waitlist = Waitlist.objects.create(restaurant=self.restaurant)
        Entry.objects.create(waitlist=waitlist, name='john doe', email='john.doe@example.com')
        Entry.objects.create(waitlist=waitlist, name='bob doe', email='bob.doe@example.com')
        entry_count = Entry.objects.count()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Waitlist.objects.count(), 1)
        self.assertEqual(entry_count, 2)
        self.assertEqual(len(response.json()['entries']), entry_count)
